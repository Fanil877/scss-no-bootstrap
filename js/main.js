$(document).ready(function () {

  $('.slider__main').slick({
    dots: true,
    infinite: false,
    speed: 300,
    slidesToShow: 1,
    slidesToScroll: 1,

  });
  $('.model__new').slick({
    dots: false,
    infinite: false,
    speed: 300,
    slidesToShow: 4,
    slidesToScroll: 4,
    responsive: [
      {
      breakpoint: 1140,
      settings: {
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        prevArrow: '<button class="arrow arrow--prev"></button>',
        nextArrow: '<button class="arrow arrow--next"></button>'
      }
    },
      {
        breakpoint: 768,
        settings: {
          infinite: true,
          slidesToShow: 2,
          slidesToScroll: 2,
          prevArrow: '<button class="arrow arrow--prev"></button>',
          nextArrow: '<button class="arrow arrow--next"></button>'
        }
      },
      {
        breakpoint: 480,
        settings: {
          infinite: true,
          slidesToShow: 1,
          slidesToScroll: 1,
          prevArrow: '<button class="arrow arrow--prev"></button>',
          nextArrow: '<button class="arrow arrow--next"></button>'
        }
      }
    ]
  });
  $('#man').on('click', function (e) {
    e.preventDefault;
    $('#man-img').toggleClass('tr');
    $('#woman-img').removeClass('tr');
    $('#child-img').removeClass('tr');
    $('.item-popup-one ').toggleClass('hide');
    $('.item-popup-two ').addClass('hide');
    $('.item-popup-three ').addClass('hide');
  })
  $('#woman').on('click', function (e) {
    e.preventDefault;
    $('#man-img').removeClass('tr');
    $('#woman-img').toggleClass('tr');
    $('#child-img').removeClass('tr');
    $('.item-popup-one ').addClass('hide');
    $('.item-popup-two ').toggleClass('hide');
    $('.item-popup-three ').addClass('hide');
  })
  $('#child').on('click', function (e) {
    e.preventDefault;
    $('#man-img').removeClass('tr');
    $('#woman-img').removeClass('tr');
    $('#child-img').toggleClass('tr');
    $('.item-popup-one ').addClass('hide');
    $('.item-popup-two ').addClass('hide');
    $('.item-popup-three ').toggleClass('hide');
  })
})
jQuery(document).ready(function () {
  jQuery('body').removeClass('loading')
      setTimeout(function () {
        jQuery('body').css('opacity', '1')
      }, 100);

  jQuery(window).scroll(hideBlock);
  function hideBlock() {
    jQuery('.menu__popup').removeClass('active');
    jQuery('.menu__burger').removeClass('menu__burger--active')
  }

  jQuery(".menu__burger").click(function (e) {
    jQuery('.menu__popup').toggleClass('active');
    jQuery('.menu__burger').toggleClass('menu__burger--active')
    e.stopPropagation();
  });
  jQuery('.menu__popup').click(function (e) {
    e.stopPropagation();
  });
  jQuery(document).click(function () {
    jQuery('.menu__popup').removeClass('active');
    jQuery('.menu__burger').removeClass('menu__burger--active')
  });

  // jQuery('.menu__popup').on('click', function () {
  //   jQuery('.menu__burger').removeClass('menu__burger--active');
  //   jQuery('.menu__popup').removeClass('active');
  // });

  // jQuery('.menu__popup_link').on('click', function (e) {
  //   e.preventDefault;
  //   jQuery('.menu__burger').removeClass('menu__burger--active');
  //   jQuery('.menu__popup').removeClass('active');
  // });
  // $('.menu__popup').on('click', function (e) {
  //   e.preventDefault;
  //   $('#image-popup-man').removeClass('tr2');
  //   $('#image-popup-woman').removeClass('tr2');
  //   $('#image-popup-child').removeClass('tr2');
  //   $('.popup_style-man').addClass('hide');
  //   $('.popup_style-woman ').addClass('hide');
  //   $('.popup_style-child ').addClass('hide');
  // })
  
  $('#man-popup').on('click', function (e) {
    e.preventDefault;
    $('#image-popup-man').toggleClass('tr2');
    $('#image-popup-woman').removeClass('tr2');
    $('#image-popup-child').removeClass('tr2');
    $('.popup_style-man').toggleClass('hide');
    $('.popup_style-woman ').addClass('hide');
    $('.popup_style-child ').addClass('hide');
  })
  $('#woman-popup').on('click', function (e) {
    e.preventDefault;
    $('#image-popup-man').removeClass('tr2');
    $('#image-popup-woman').toggleClass('tr2');
    $('#image-popup-child').removeClass('tr2');
    $('.popup_style-man').addClass('hide');
    $('.popup_style-woman').toggleClass('hide');
    $('.popup_style-child').addClass('hide');
  })
  $('#child-popup').on('click', function (e) {
    e.preventDefault;
    $('#image-popup-man').removeClass('tr2');
    $('#image-popup-woman').removeClass('tr2');
    $('#image-popup-child').toggleClass('tr2');
    $('.popup_style-man').addClass('hide');
    $('.popup_style-woman').addClass('hide');
    $('.popup_style-child').toggleClass('hide');
  })
})